namespace TaskChainingAndContinuation.Tests
{
    using System;
    using NUnit.Framework;

    /// <summary>
    /// Test class.
    /// </summary>
    [TestFixture]
    public class ChainingAndContinuationTests
    {
        private const int MultiplicationFactor = 10;
        private static readonly int[] EmptyArray = Array.Empty<int>();
        private static readonly int[] PreFilledArray = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        /// <summary>
        /// Test if passing empty array throws an exception.
        /// </summary>
        [Test]
        public void MultipliesArrayByRandomNumber_Throws_ArgumentException_When_Empty_Array_Is_passed()
        {
            Assert.Throws<ArgumentException>(() => ChainingAndContinuation.MultipliesArrayByRandomNumber(EmptyArray, MultiplicationFactor));
        }

        /// <summary>
        /// Testing if passing null throws an exception.
        /// </summary>
        [Test]
        public void MultipliesArrayByRandomNumber_Throws_ArgumentException_When_Null_Array_Is_passed()
        {
            Assert.Throws<ArgumentNullException>(() => ChainingAndContinuation.MultipliesArrayByRandomNumber(null, MultiplicationFactor));
        }

        /// <summary>
        /// Testing MultipliesArrayByRandomNumber method.
        /// </summary>
        [Test]
        public void MultipliesArrayByRandomNumber_Returns_Array()
        {
            var expected = new int[] { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };

            var act = ChainingAndContinuation.MultipliesArrayByRandomNumber(PreFilledArray, MultiplicationFactor);

            Assert.That(act, Is.EqualTo(expected));
        }

        /// <summary>
        /// Test if passing empty array throws an exception.
        /// </summary>
        [Test]
        public void SortArrayByAscending_Throws_ArgumentException_When_Empty_Array_Is_passed()
        {
            Assert.Throws<ArgumentException>(() => ChainingAndContinuation.SortArrayByAscending(EmptyArray));
        }

        /// <summary>
        /// Testing if passing null throws an exception.
        /// </summary>
        [Test]
        public void SortArrayByAscending_Throws_ArgumentException_When_Null_Array_Is_passed()
        {
            Assert.Throws<ArgumentNullException>(() => ChainingAndContinuation.SortArrayByAscending(null));
        }

        /// <summary>
        /// Testing SortArrayByAscending method.
        /// </summary>
        [Test]
        public void SortArrayByAscending_Returns_Array()
        {
            var expected = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            var act = ChainingAndContinuation.SortArrayByAscending(PreFilledArray);

            Assert.That(act, Is.EqualTo(expected));
        }

        /// <summary>
        /// Test if passing empty array throws an exception.
        /// </summary>
        [Test]
        public void AverageOfTheArrayElements_Throws_ArgumentException_When_Empty_Array_Is_passed()
        {
            Assert.Throws<ArgumentException>(() => ChainingAndContinuation.AverageOfTheArrayElements(EmptyArray));
        }

        /// <summary>
        /// Testing if passing null throws an exception.
        /// </summary>
        [Test]
        public void AverageOfTheArrayElements_Throws_ArgumentException_When_Null_Array_Is_passed()
        {
            Assert.Throws<ArgumentNullException>(() => ChainingAndContinuation.AverageOfTheArrayElements(null));
        }

        /// <summary>
        /// Testing AverageOfTheArrayElements method.
        /// </summary>
        [Test]
        public void AverageOfTheArrayElements_Returns_Array()
        {
            var expected = 5.5;

            var act = ChainingAndContinuation.AverageOfTheArrayElements(PreFilledArray);

            Assert.That(act, Is.EqualTo(expected));
        }
    }
}