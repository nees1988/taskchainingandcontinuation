﻿namespace TaskChainingAndContinuation
{
    /// <summary>
    /// Class for chaining tasks.
    /// </summary>
    public static class ChainingAndContinuation
    {
        private static readonly Random Random = new ();
        private static readonly object DisplayLock = new ();

        /// <summary>
        /// Multiply array elements by random number.
        /// </summary>
        /// <param name="arr">Array.</param>
        /// <param name="number">Random number.</param>
        /// <returns>Integer array.</returns>
        public static int[] MultipliesArrayByRandomNumber(int[] arr, int number)
        {
            ExceptionHelper.ArrayIsNullOrEmpty(arr);

            var result = arr.Select(x => x * number).ToArray();
            return result;
        }

        /// <summary>
        /// Sort array in ascending order.
        /// </summary>
        /// <param name="arr">Array.</param>
        /// <returns>Sorted array.</returns>
        public static int[] SortArrayByAscending(int[] arr)
        {
            ExceptionHelper.ArrayIsNullOrEmpty(arr);

            var result = arr.OrderBy(x => x).ToArray();
            return result;
        }

        /// <summary>
        /// Take average of array elements.
        /// </summary>
        /// <param name="arr">Array.</param>
        /// <returns>Average.</returns>
        public static double AverageOfTheArrayElements(int[] arr)
        {
            ExceptionHelper.ArrayIsNullOrEmpty(arr);

            var result = arr.Average();
            return result;
        }

        /// <summary>
        /// Chain tasks.
        /// </summary>
        /// <param name="arrayLenght">Array length.</param>
        /// <param name="from">Array element lower bound.</param>
        /// <param name="to">Array element upper bound.</param>
        /// <returns>Average result.</returns>
        public static double ChainTasks(int arrayLenght, int from, int to)
        {
            ExceptionHelper.ValueLessThanDefined(arrayLenght, 1);
            ExceptionHelper.ValueLessThanDefined(to, from);

            var taskCreateArray = Task.Factory.StartNew(() => CreateArrayOfRandomIntegers(from, to, arrayLenght), CancellationToken.None, TaskCreationOptions.None, TaskScheduler.Default);
            var taskPrint = taskCreateArray.ContinueWith(x => Print(x.Result, "Array: "), TaskScheduler.Default);

            var taskGenerateRandomNumber = Task.Factory.StartNew(() => GenerateRandomInteger(from, to), CancellationToken.None, TaskCreationOptions.None, TaskScheduler.Default);
            var taskPrintRandomInteger = taskGenerateRandomNumber.ContinueWith(x => ShowMessage($"Generated random number is {x.Result}"), TaskScheduler.Default);

            var taskCombined = Task.WhenAll(taskCreateArray, taskGenerateRandomNumber);
            var taskMultipliesArrayByRandomNumber = taskCombined.ContinueWith(t => MultipliesArrayByRandomNumber(taskCreateArray.Result, taskGenerateRandomNumber.Result), TaskScheduler.Default);
            var taskPrintMultipliedArrayByNumber = taskMultipliesArrayByRandomNumber.ContinueWith(x => Print(x.Result, "Multiplied array by random number"), TaskScheduler.Default);

            var taskSortArrayByAscending = taskMultipliesArrayByRandomNumber.ContinueWith(x => SortArrayByAscending(x.Result), TaskScheduler.Default);
            var taskPrintSortedArrayByAscending = taskSortArrayByAscending.ContinueWith(x => Print(x.Result, "Ordered array"), TaskScheduler.Default);

            var taskCalculateAverage = taskMultipliesArrayByRandomNumber.ContinueWith(x => AverageOfTheArrayElements(x.Result), TaskScheduler.Default);
            var taskPrintAverage = taskCalculateAverage.ContinueWith(x => ShowMessage($"Average value is {x.Result}"), TaskScheduler.Default);

            Task.WaitAll(
                taskPrint,
                taskPrintRandomInteger,
                taskPrintMultipliedArrayByNumber,
                taskPrintSortedArrayByAscending,
                taskPrintAverage);

            return taskCalculateAverage.Result;
        }

        /// <summary>
        /// Create array of random integers.
        /// </summary>
        /// <param name="from">Array element lower bound.</param>
        /// <param name="to">Array element upper bound.</param>
        /// <param name="arrayLength">Array length.</param>
        /// <returns>Array.</returns>
        private static int[] CreateArrayOfRandomIntegers(int from, int to, int arrayLength)
        {
            ExceptionHelper.ValueLessThanDefined(arrayLength, 1);
            ExceptionHelper.ValueLessThanDefined(to, from);

            var arr = new int[arrayLength];
            for (int i = 0; i < arrayLength; i++)
            {
                arr[i] = GenerateRandomInteger(from, to);
            }

            return arr;
        }

        /// <summary>
        /// Generate random integers.
        /// </summary>
        /// <param name="from">Array element lower bound.</param>
        /// <param name="to">Array element upper bound.</param>
        /// <returns>Random integer.</returns>
        private static int GenerateRandomInteger(int from, int to)
        {
            ExceptionHelper.ValueLessThanDefined(to, from);

            lock (Random)
            {
                return Random.Next(from, to + 1);
            }
        }

        /// <summary>
        /// Print array.
        /// </summary>
        /// <param name="arr">Array.</param>
        /// <param name="title">Title.</param>
        private static void Print(int[] arr, string title)
        {
            ExceptionHelper.StringNullOrEmpty(title);
            ExceptionHelper.ArrayIsNullOrEmpty(arr);

            lock (DisplayLock)
            {
                Console.WriteLine($"\n{title}");
                for (int i = 0; i < arr.Length; i++)
                {
                    Console.WriteLine($"\t{arr[i]}");
                }
            }
        }

        /// <summary>
        /// Show message.
        /// </summary>
        /// <param name="message">Message.</param>
        private static void ShowMessage(string message)
        {
            ExceptionHelper.StringNullOrEmpty(message);

            lock (DisplayLock)
            {
                Console.WriteLine($"\n{message}");
            }
        }
    }
}
