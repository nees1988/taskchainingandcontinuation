﻿namespace TaskChainingAndContinuation
{
    /// <summary>
    /// Start of the program.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Program entry point.
        /// </summary>
        public static void Main()
        {
            var result = ChainingAndContinuation.ChainTasks(10, 1, 5);
            Console.WriteLine($"\nResult {result}");
        }
    }
}
